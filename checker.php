<?php
/**
 * Скрипт ищет в свежих исходниках сайта whatsapp id модуля, отвечающего за загрузки картинок.
 * Если не работает отправка изображений, то нужно проверить в браузере переменую window.MediaConst -
 * это должен быть конструктор с методом processFile. В случае отсутсвия кнструктора, или используется другой конструктор,
 * то нужно проверить подключение в конце файла view/hack.js.twig . Id должен совпадать с ID который находит checker.php.
 * P.S. Регулярка не всегда правльно находит нужный id. Иногда она берет предыдущий, тогда нужный нужно найти в теле.
 */

require_once 'vendor/autoload.php';

$host    = 'https://web.whatsapp.com/';
$headers = [
    'headers' => [
        'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36'
    ]
];
$client  = new \GuzzleHttp\Client();
$site    = $client->get($host, $headers);
$found   = preg_match('/(progress\..+?\.js)/', $site->getBody()->getContents(), $matches);

if (!$found) {
    echo "Cannot find progress js\n";
    exit(-1);
} else {
    echo sprintf("Actual progress.js is %s \n", $matches[0]);
}

$progress = $client->get($host . $matches[0], $headers);
$found    = preg_match('/(app2\..+?\.js)/', $progress->getBody()->getContents(), $matches);

if (!$found) {
    echo "Cannot find app2 js\n";
    exit(-1);
} else {
    echo sprintf("Actual app2.js is %s \n", $matches[0]);
}

$app   = $client->get($host . $matches[0], $headers);
$found = preg_match('/\'("\w+?")\':function.+?processFiles:/', $app->getBody()->getContents(), $matches);

if (!$found) {
    echo "Cannot find actual media index\n";
} else {
    preg_match_all('/\'("\w+?")\':function/', $matches[0], $indexes);
    $index = array_pop($indexes[1]);
    echo sprintf("Actual media index is %s \n", $index);
}