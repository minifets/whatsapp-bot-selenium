#!/usr/bin/env php
<?php

use App\Container;
use App\Controller;
use GuzzleHttp\Client;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Psr\Http\Message\ServerRequestInterface;
use React\EventLoop\Factory;
use React\Http\Response;
use React\Http\Server as HttpServer;
use React\Promise\Promise;
use React\Socket\Server as SocketServer;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\HttpFoundation\ParameterBag;

require_once __DIR__ . '/config.php';
require_once __DIR__ . '/vendor/autoload.php';

/** @var ArgvInput $input console input arguments */
$input = new ArgvInput();

/**
 * Главный цикл ReactPHP
 */
Container::set('loop', $loop = Factory::create());
/**
 * Клиент для HTTP запросов.
 */
Container::set('http_client', new Client());
/**
 * Ротационный логгер по дням и порту. Название файла имееь вид log-порт-дата.txt
 */
Container::set('logger', $logger = new Logger('server', [
    new RotatingFileHandler(
        str_replace('::port::', $input->getParameterOption(['--port', '-p'], DEFAULT_PORT), LOG_FILE),
        30,
        ENV === 'dev' ? Logger::DEBUG : Logger::WARNING
    ),
]));
/**
 * Инициализируем sqlite базу для хранения кеша сообщений.
 */
Container::set('db', $db = new PDO('sqlite:db/cache.db'));
try {
    $db->beginTransaction();
    $db->exec("CREATE TABLE IF NOT EXISTS messages (
`id` VARCHAR(255), 
`me` BOOLEAN,
`sender` VARCHAR(30),
`to` VARCHAR(30),
`type` VARCHAR(10),
`ack` INTEGER,
`instance` VARCHAR(255),
`cached_at` DATETIME DEFAULT CURRENT_TIMESTAMP, 
`body` TEXT)");
    $db->commit();
} catch (Exception $e) {
    $logger->error($e->getMessage());

    $db->rollBack();
}

/**
 * Twig шаблонизатор для js скрипта, который инжектится в web.whatsapp
 */
Container::set('twig', new Twig_Environment(new Twig_Loader_Filesystem(VIEW_DIR)));
$controller = new Controller();
/**
 * Доступ к процессу можно обезопасить с помощью указания токена авторизации.
 */
$token  = $input->getParameterOption(['--token', '-t'], null);
$server = new HttpServer(function (ServerRequestInterface $request) use ($controller, $logger, $token) {
    if ($token and $token !== $request->getHeaderLine('X-Auth-Token')) {
        return new Response(
            403,
            ['Content-Type' => 'application/json'],
            json_encode("Access denied")
        );
    }

    /**
     * Формируем пакет параметров из get, post и json данных, которые передаются в урл.
     */
    $bag = new ParameterBag();
    $bag->add($request->getQueryParams());
    $bag->add((array)$request->getParsedBody());
    if ($request->hasHeader('Content-Type') and $request->getHeaderLine('Content-Type') == 'application/json') {
        $bag->add((array)json_decode($request->getBody()->getContents(), true));
    }

    /**
     * Находим и вызываем необходимый action.
     */
    return new Promise(function ($resolve) use ($request, $bag, $controller, $logger) {
        $action = ltrim($request->getRequestTarget(), '/') . "Action";
        if (!method_exists($controller, $action)) {
            $logger->warning("Request in not supported action $action.", [$request->getServerParams()['REMOTE_ADDR']]);
            $resolve(new Response(404, ['Content-Type' => 'application/json']));
        } else {
            $logger->debug("New request to $action", [$request->getServerParams()['REMOTE_ADDR']]);
            $bag->set('resolver', $resolve);
            $response = call_user_func_array([$controller, $action], [$request, $bag]);
            if ($response instanceof Response) {
                $resolve($response);
            }
        }
    });
});

/**
 * Запускаем приложение.
 */
$address = sprintf(
    '%s:%s',
    $input->getParameterOption(['--host', '-h'], DEFAULT_HOST),
    $input->getParameterOption(['--port', '-p'], DEFAULT_PORT)
);
$socket  = new SocketServer($address, $loop);
$server->listen($socket);
$loop->run();