<?php

require_once __DIR__ . '/../vendor/autoload.php';

$conn   = mysqli_connect('127.0.0.1', 'root', 'root', 'test');
$table  = 'stats';

$client = new \GuzzleHttp\Client();
$response = $client->get('http://localhost:8080/newMessages');

$messages = json_decode($response->getBody()->getContents(), true);

foreach ($messages as $message) {
    if ($message['me']) {
        mysqli_query($conn, "UPDATE $table SET `status`={$message['ack']} WHERE phone = '{$message['to']}'");
    } else {
        $comments = mysqli_query($conn, "SELECT comments FROM $table WHERE phone = '{$message['sender']}'")->fetch_assoc();
        $data = (array) json_decode($comments['comments'], true);
        array_push($data, $message['body']);
        $json = json_encode($data);

        mysqli_query($conn, "UPDATE $table SET comments='$json' WHERE phone = '{$message['sender']}'");
    }
}