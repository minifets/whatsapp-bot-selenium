<?php

$phones = file_get_contents(__DIR__ . '/phones1');
$result = '';
$array  = array_unique(explode("\n", $phones));
file_put_contents(__DIR__ . '/phones', implode("\n", $array));

foreach ($array as $phone) {
    $result .= sprintf("BEGIN:VCARD
VERSION:2.1
N;CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:$phone
TEL;CELL:+$phone
END:VCARD
");
}

file_put_contents(__DIR__ . '/00002.vcf', $result);