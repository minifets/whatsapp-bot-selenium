<?php

require_once __DIR__ . '/../vendor/autoload.php';

$client     = new \GuzzleHttp\Client();
$message    = file_get_contents(__DIR__ . '/message');
$phones     = file_get_contents(__DIR__ . '/phone1');

foreach (explode("\n", $phones) as $phone) {
    $client->post('http://localhost:8020/send', [
        'json' => [
            'to'    => $phone,
            'text'  => $message
        ]
    ]);
    sleep(1);
}
