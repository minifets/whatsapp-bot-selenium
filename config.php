<?php

define('ENV', 'dev');

// Server settings
define('DEFAULT_HOST', '0.0.0.0');
define('DEFAULT_PORT', 8080);
define('DEFAULT_REALM', 'Protected zone');

define('BASE_DIR', __DIR__);
define('VIEW_DIR', __DIR__ . '/view');
define('LOG_FILE', __DIR__ . '/logs/log-::port::.txt');