<?php


namespace Bot;

use App\Container;
use Facebook\WebDriver\Chrome\ChromeDriver;
use Facebook\WebDriver\Chrome\ChromeDriverService;
use Facebook\WebDriver\Exception\WebDriverException;
use Facebook\WebDriver\WebDriverBy;
use JBZoo\Image\Exception;
use JBZoo\Image\Image;
use Monolog\Logger;

/**
 * Scripts for Selenium web driver
 *
 * @package Bot
 */
class Whatsapp
{
    /**
     * Selenium port
     *
     * @var int
     */
    public static $port = 9515;

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $token;

    /**
     * @var ChromeDriver
     */
    protected $driver;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var array
     */
    protected $timers = [];

    /**
     * Whatsapp constructor.
     */
    public function __construct()
    {
        $this->logger = Container::get('logger');
    }

    /**
     * Init new selenium instance
     *
     * @param int $id Driver ID
     *
     * @return bool|string Bot id on success and false on failure
     */
    public function init($id = null)
    {
        $this->id     = $id;
        $service      = new ChromeDriverService(
            realpath(__DIR__ . '/chromedriver'),
            self::$port,
            ["--port=" . self::$port]
        );
        $this->driver = ChromeDriver::start(null, $service);
        $this->logger->debug("Start new selenium on port " . self::$port);

        // Open WhatsApp site
        $this->driver->get('https://web.whatsapp.com/');
        // Wait 10 seconds after page loaded
        sleep(10);

        try {
            /** @var \Twig_Environment $twig */
            $twig = Container::get('twig');
            $this->driver->executeScript($twig->render('hack.js.twig'));
        } catch (\Exception $ex) {
            $this->logger->error($ex->getMessage());

            return false;
        }

        return strtoupper(md5(random_bytes(30)));
    }

    /**
     * Close Driver
     */
    public function close()
    {
        $this->driver->close();
    }

    /**
     * Check login qr code
     *
     * @return bool
     */
    public function isLogged()
    {
        try {
            $this->driver->findElement(WebDriverBy::cssSelector('img[alt="Scan me!"]'));
        } catch (WebDriverException $ex) {
            //$this->logger->error($ex->getMessage());
            return true;
        }

        return false;
    }

    /**
     * Get Qr code for login
     *
     * @return string
     */
    public function getQrCode()
    {
        try {
            // Reload code before and wait loading
            if ($this->reloadQrCode()) {
                sleep(3);
            }
            $image = $this->driver->findElement(WebDriverBy::tagName('img'));
        } catch (WebDriverException $ex) {
            //$this->logger->error($ex->getMessage());
            return null;
        }

        return $image->getAttribute('src');
    }

    /**
     * Reload qr code
     *
     * @return bool
     */
    protected function reloadQrCode()
    {
        try {
            $button = $this->driver->findElement(WebDriverBy::className('HnNfm'));
            $button->click();
        } catch (WebDriverException $ex) {
            return false;
        }

        return true;
    }

    /**
     * Get driver phone number
     *
     * @return string|null
     */
    public function getMe()
    {
        try {
            return $this->driver->executeScript('return cmd.getMe();');
        } catch (WebDriverException $e) {
            $this->logger->error($e->getMessage());

            return null;
        }
    }

    /**
     * Check phone connection
     *
     * @return bool
     */
    public function isConnected()
    {
        try {
            return $this->driver->executeScript('return cmd.isConnected();');
        } catch (WebDriverException $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }

    /**
     * Get contact list
     *
     * @return array
     */
    public function getContacts()
    {
        try {
            return $this->driver->executeScript('return cmd.getContacts();');
        } catch (WebDriverException $e) {
            $this->logger->error($e->getMessage());

            return [];
        }
    }

    /**
     * Get chat list
     *
     * @return array
     */
    public function getChats()
    {
        try {
            return $this->driver->executeScript('return cmd.getChats();');
        } catch (WebDriverException $e) {
            $this->logger->error($e->getMessage());

            return [];
        }
    }

    /**
     * Get list of all new messages
     *
     * @return array
     */
    public function getNewMessages()
    {
        try {
            return $this->driver->executeScript(sprintf('return awUYFfyv.get_events();'));
        } catch (WebDriverException $e) {
            $this->logger->error($e->getMessage());

            return [];
        }
    }

    /**
     * @param string $to
     * @param string $msg
     *
     * @return bool|mixed
     */
    public function sendMessage($to, $msg)
    {
        try {
            return $this->driver->executeScript(sprintf('awUYFfyv.execute_commands(%s)', json_encode([[
                'cmd' => 'send',
                'abonent' => $to,
                'content' => $msg
            ]])));
        } catch (WebDriverException $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }

    /**
     * Send image to chat
     *
     * @param string $to
     * @param string $image
     * @param string $caption
     *
     * @return bool|mixed
     */
    public function sendImage($to, $image, $caption = null)
    {
        try {
            $image = (new Image($image))
                ->fitToHeight(800)
                ->fitToWidth(800)
                ->getBase64('jpg', 100, false);

            return $this->driver->executeScript(sprintf('awUYFfyv.execute_commands(%s)', json_encode([[
                'cmd' => 'send',
                'type' => 'image',
                'abonent' => $to,
                'content' => $image,
                'caption' => $caption
            ]])));
        } catch (WebDriverException $e) {
            $this->logger->error($e->getMessage());

            return false;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        } catch (\JBZoo\Utils\Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }
}