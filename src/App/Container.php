<?php


namespace App;

/**
 * App service container
 *
 * @package App
 */
class Container
{
    /**
     * @var array
     */
    protected static $services = [];

    /**
     * Get service from container
     *
     * @param string $name
     *
     * @return mixed|null
     */
    public static function get($name)
    {
        if (array_key_exists($name, self::$services)) {
            return self::$services[$name];
        }

        return null;
    }

    /**
     * Set service to container
     *
     * @param string $name
     * @param mixed $service
     */
    public static function set($name, $service)
    {
        self::$services[$name] = $service;
    }
}