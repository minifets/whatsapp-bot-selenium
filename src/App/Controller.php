<?php


namespace App;

use Bot\Whatsapp;
use GuzzleHttp\Client;
use Monolog\Logger;
use Psr\Http\Message\ServerRequestInterface;
use React\Http\Response;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * App controller class
 *
 * @package App
 */
class Controller
{
    /**
     * @var Whatsapp[]
     */
    protected $bots = [];

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Client
     */
    protected $http;

    /**
     * Проверить работоспособность сервера
     *
     * @return Response
     */
    public function pingAction()
    {
        return $this->renderJson(date('U'));
    }

    /**
     * Init new bot
     *
     * @return Response
     */
    public function initAction()
    {
        $bot = new Whatsapp();

        if (!$token = $bot->init()) {
            return $this->renderJson(['status' => -1]);
        }

        $this->bots[$token] = $bot;
        $this->getLogger()->debug(sprintf('Create new bot with token `%s`', $token));

        return $this->renderJson(['status' => 1, 'token' => $token]);
    }

    /**
     * Close opened bot
     *
     * @param ServerRequestInterface $request Параметры, которые приходят в запросе
     * @return Response
     */
    public function closeAction(ServerRequestInterface $request)
    {
        $token = $request->getHeaderLine('X-Bot-Token');
        $bot   = $this->getBot($token);

        if (!$bot instanceof Whatsapp) {
            return $this->renderJson(['status' => -1]);
        }

        $bot->close();
        if (empty($token)) {
            array_shift($this->bots);
        } else {
            unset($this->bots[$token]);
        }

        return $this->renderJson(['status' => 1]);
    }

    /**
     * Get qr code for login
     *
     * @param ServerRequestInterface $request
     * @return Response
     */
    public function loginAction(ServerRequestInterface $request)
    {
        $bot = $this->getBot($request->getHeaderLine('X-Bot-Token'));

        return $this->renderJson([
            'logged' => $bot->isLogged(),
            'qrcode' => $bot->getQrCode(),
            'me'     => $bot->getMe()
        ]);
    }

    /**
     * Get contact list
     *
     * @param ServerRequestInterface $request
     * @return Response
     */
    public function contactsAction(ServerRequestInterface $request)
    {
        return $this->renderJson($this->getBot($request->getHeaderLine('X-Bot-Token'))->getContacts());
    }

    /**
     * Get chat list
     *
     * @param ServerRequestInterface $request
     * @return Response
     */
    public function chatsAction(ServerRequestInterface $request)
    {
        return $this->renderJson($this->getBot($request->getHeaderLine('X-Bot-Token'))->getChats());
    }

    /**
     * Send message to chat
     *
     * @param ServerRequestInterface $request
     * @param ParameterBag $params
     *
     * @return Response
     */
    public function sendAction(ServerRequestInterface $request, ParameterBag $params)
    {
        return $this->renderJson($this->sendMessage(
            $params->get('to'),
            $params->get('text'),
            $params->get('image'),
            $params->get('type', 'chat'),
            $this->getBot($request->getHeaderLine('X-Bot-Token'))
        ));
    }

    /**
     * Send multi messages to one chat
     *
     * @param ServerRequestInterface $request
     * @param ParameterBag $params
     *
     * @return Response
     */
    public function multiSendAction(ServerRequestInterface $request, ParameterBag $params)
    {
        $to  = $params->get('to');
        $bot = $this->getBot($request->getHeaderLine('X-Bot-Token'));

        foreach ($params->get('messages') as $message) {
            $this->sendMessage(
                $to,
                $message['text'] ?? null,
                $message['image'] ?? null,
                $message['type'] ?? 'chat',
                $bot
            );
        }

        return $this->renderJson(['status' => 1]);
    }

    /**
     * Send message to bot
     *
     * @param string $to
     * @param string $text
     * @param string $image
     * @param string $type
     * @param Whatsapp $bot
     *
     * @return array
     */
    protected function sendMessage($to, $text, $image, $type, Whatsapp $bot)
    {
        // Clean up text from excess characters and spaces
        $text = strtr($text, ["\n" => '\n', '"' => "'"]);
        $text = preg_replace('/\s{4,}/', '\n', $text);
        $text = preg_replace('/\p{Cc}+/u', '', $text);

        // Send a message depending on the type
        switch ($type) {
            case "image":
                // Download image from url
                if (filter_var($image, FILTER_VALIDATE_URL)) {
                    try {
                        $response = $this->getHttp()->get($image);
                        $image    = $response->getBody()->getContents();
                    } catch (\Exception $e) {
                        $this->getLogger()->error($e->getMessage());

                        return [
                            'status' => -1,
                            'error'  => [
                                'code' => $e->getCode(),
                                'msg'  => $e->getMessage(),
                            ],
                        ];
                    }
                }

                $status = $bot->sendImage($to, $image, $text ?: null);
                break;
            case "chat":
            default:
                $status = $bot->sendMessage($to, $text);
        }

        return $status;
    }

    /**
     * Get new messages
     *
     * @param ServerRequestInterface $request
     * @return Response
     */
    public function newMessagesAction(ServerRequestInterface $request)
    {
        $result = [];
        /** @var \PDO $db */
        $db = Container::get('db');

        foreach ($this->bots as $id => $bot) {
            $messages = array_map(function (array $message) use ($id) {
                $message['instance'] = $id;
                return $message;
            }, $bot->getNewMessages());
            $result   = array_merge($result, $messages);
        }

        if (!empty($result)) {
            $query = $db->prepare("INSERT INTO messages(`id`, `me`, `sender`, `to`, `type`, `ack`, `instance`, `body`) VALUES (:id, :me, :sender, :to, :type, :ack, :instance, :body)");

            foreach ($result as $message) {
                $query->execute($message);
            }
        }

        return $this->renderJson($result);
    }

    /**
     * Execute all cached messages
     *
     * @param ServerRequestInterface $request
     *
     * @param ParameterBag $params
     * @return Response
     */
    public function cachedMessagesAction(ServerRequestInterface $request, ParameterBag $params)
    {
        /** @var \PDO $db */
        $db = Container::get('db');

        if ($params->has('id')) {
            $query = $db->prepare('SELECT * FROM messages WHERE instance = ?');
            $query->execute([$params->get('id')]);
        } else {
            $query = $db->prepare('SELECT * FROM messages');
            $query->execute();
        }

        $messages = $query->fetchAll(\PDO::FETCH_ASSOC);

        return $this->renderJson($messages);
    }

    /**
     * Remove message from cache by id
     *
     * @param ServerRequestInterface $request
     * @param ParameterBag $params
     *
     * @return Response
     */
    public function clearMessageAction(ServerRequestInterface $request, ParameterBag $params)
    {
        /** @var \PDO $db */
        $db = Container::get('db');

        $status = $db
            ->prepare('DELETE FROM messages WHERE id = ?')
            ->execute([$params->get('id')]);

        return $this->renderJson(['success' => $status]);
    }

    /**
     * Проверить всех ботов на подключение и статус.
     */
    public function checkBotsAction()
    {
        $result = [];
        foreach ($this->bots as $id => $bot) {
            $result[$id] = [
                'logged'   => $bot->isLogged(),
                'worked'   => boolval($bot->getMe()),
                'internet' => $bot->isConnected()
            ];
        }

        return $this->renderJson($result);
    }

    /**
     * Get bot instance
     *
     * @param string $id
     * @return Whatsapp|false
     */
    protected function getBot($id = null)
    {
        if (empty($id) or !array_key_exists($id, $this->bots)) {
            return reset($this->bots);
        }

        return $this->bots[$id];
    }

    /**
     * Get logger
     *
     * @return Logger
     */
    protected function getLogger()
    {
        if (!$this->logger) {
            $this->logger = Container::get('logger');
        }

        return $this->logger;
    }

    /**
     * Get http client for requests
     *
     * @return Client
     */
    protected function getHttp()
    {
        if (!$this->http) {
            $this->http = Container::get('http_client');
        }

        return $this->http;
    }

    /**
     * Render json response
     *
     * @param mixed $data
     * @param int $code
     * @param array $headers
     *
     * @return Response
     */
    protected function renderJson($data, $code = 200, $headers = [])
    {
        return new Response(
            $code,
            array_merge($headers, ['Content-Type' => 'application/json']),
            json_encode($data)
        );
    }
}